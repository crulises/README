---
Title: Sublime Text Editor
Subtitle: The Minimal Editor That Started It All
Query: true
---

The [Sublime Text](https://www.sublimetext.com) editor started the revolution in light-weight [GUI](/what/hci/ui/graphic/) code editing tools over heavier [IDEs](https://duck.com/lite?kae=t&q=IDEs). This let to [Atom](/tools/editors/atom/), which brought us [Electron](/what/electron/), and eventually [VSCode](/tools/editors/vscode/).
