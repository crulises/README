---
Title: Atom Text Editor
Query: true
---

The [Atom](https://atom.io) editor was [GitHub](/services/github/)'s answer to the revolutionary [Sublime](/tools/editors/sublime/) light-weight text editor. The [Electron](/what/electron/) framework was built specifically *for* [Atom](/tools/editors/atom/). The goal was to use *only* web technology instead of C++ and Python which is what Sublime is built on. While many people prefer Atom today over the far more popular and powerful [VSCode](/tools/editors/vscode/) it lacks some rather significant necessities according to most developers. Atom is not recommended for this course mostly because we are focusing on editing code with [Vim](/tools/editors/vi/) above all and secondarily with web tools and VSCode.
