---
Title: cURL Command Line Internet Client
Subtitle: Web's More Important Library and Command Tool
Query: true
---

*cURL* is the name of a software library used by pretty much every single program on the planet that access any Internet service. It is the most significant software package of its kind. It is also the name of the `curl` utility command that comes with the library. The `curl` command comes on most all systems by default these days.

:::co-faq

## Why not `wget`?

Curl is far more powerful for most use cases involving interacting with *any* [Internet application layer protocol.](https://duck.com/lite?kae=t&q=Internet application layer protocol.) The `wget` tool is specifically for use on the Web and can crawl and download entire sites, but its use within scripts is hindered by its lack of clean support for dealing directly with the [HTTP protocol](https://duck.com/lite?kae=t&q=HTTP protocol) making it less useful overall. It's better to only learn `curl` *until* you might need a tool to do what `curl` can't.

:::
