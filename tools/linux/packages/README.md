---
Title: Linux Package Managers
Query: true
---

### Debian Package Manager

* `apt`
* `dpkg`

### RedHat Package Manager

* `yum`

### Arch User

###  SUSE

* `yast`

### GUIX

### Brew

### Chocolatey

## Language

* cabal 
* 

## Resource

[Linux Package Management](https://www.linode.com/docs/tools-reference/linux-package-management/)


