---
Title: You Don't Know JavaScript
Subtitle: Skip This Annoyingly Bad Book and Author
Query: true
---

1. It's not *fully* free (despite the claims of the author).
1. It's not a beginner book.
1. It's often just plain wrong.
1. The author is an annoying asshole.

All you really need to know about this is the title. Any author who would actually name a book like that is exactly what you would expect, a condescending, chin-bearded asshole. Please don't give this person your money or attention in any form. His attitude and educational approach espouse the worst tech behaviors and attitudes that the RWX community is directly fighting against.

Besides, most of the stuff covered in the book is not stuff you *ever* need to worry about if you simple learn *modern* JavaScript properly. Save yourself the cognitive overhead and skip this. If you want more, read [Eloquent JavaScript](/reviews/books/eloqjs/) or [Learning JavaScript](/reviews/books/ljs/) instead.

