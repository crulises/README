---
Title: Linux Terminal Mastery
Subtitle: Power of Bash, TMUX, Vi/m, cURL, and Lynx Used *Together*
Query: true
---

A *terminal* is a text [user interface](/what/hci/ui/) to a computer. Most terminal users will spend most of their time on the [command line](/what/hci/ui/graphic/) but will also use [terminal user interface](https://duck.com/lite?kae=t&q=terminal user interface) applications such as [Vi](/tools/editors/vi/). Like a professional pianist who learns proper hand position, a home-row terminal master with well-tuned terminal instruments seems like a virtuoso compared to others.

## Get Me a Terminal, Stat{#get}

Here are the fastest ways to get a true Linux Bash terminal running:

  OS         Method
------------ -------------------------------------------------------------------------
 Windows 10  Install [Ubuntu WSL](/tools/linux/distros/wsl/) and Windows Terminal Preview
 Mac         Install Docker and run Linux container
 Linux       Open one

If you are online and away from one of these methods you can always just the *Shell* from [PicoCTF](https://picoctf.com) assuming you are using it for the CTF as well (otherwise it would be a violation of their terms).

You might also consider installing [Alacritty](https://github.com/alacritty/alacritty).

## Most Direct Human-Computer Interface

Terminal masters get most things done faster than those bound to a  [graphic interface](/what/hci/ui/graphic/). It's just physics. You simply cannot rename 100 files as quickly with a graphics UI. Searching the Web from a terminal takes seconds to find stuff that takes *average* graphics user three minutes to find and do it with 1/100th the RAM and CPU utilization.

:::co-comment
Computer languages designed primarily to be written quickly, like Bash and Perl, are often maligned for being so weird looking and "unreadable." But the denseness of these languages is because they are highly contextual, a single symbol can mean something different when used in a different context. This enables the languages to be much more useful directly from the command line. Compare, for example, the inability to do similar things in Python from the command line *because* of its dubious decision to *require* white-space instead of semicolons to end lines.
:::

## Masters

For decades this workflow has been something of a secret passed on from master to apprentice. This is unfortunate because so many simply do not even know what is possible because they have not experienced it. Once they do, almost all of them prefer it. There's something about seeing  someone operate a terminal by sheer force of will that makes people really want that power, like watching Dumbledore. 

## Cynics

A lot of inexperienced, slow, graphics-dependent technologists will try and convince you that this stuff is all just "old fashioned" --- they might even cluelessly claim that Vi is an "ancient" editor implying that no one needs to use it. Thankfully an *overwhelming* number of young, intelligent, discerning people have discovered and tested the merits of these workflows and would laugh in their faces if they ever took the time.
