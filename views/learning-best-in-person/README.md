# Learning Best in Person

Countless studies have shown that one-on-one learning in person from someone who has mastered the [skill](/skill), [knowledge](/knowledge), and [abilities](/ability)—and is minimally able to  communicate—is most effective. A degree in teaching is not required. A teaching certificate is not required. What *is* required is personally mastery *before* attempting to teach it. This is the foundation of the [guru](/guru) model of education used for thousands of years before "teaching" was even a word.

## Problem with Online Learning

Online learning will *never* be as good as sitting beside the person.

Learning online is a relatively new thing on the time scale of human learning so there is not a lot of science on it, yet. Clearly the availability of content alone has dramatically increased learning opportunities, much like having a local library did before. People learned because they *want* to learn and have a source. The source can be online, in a library, but is best from another human who has mastered what the learner seeks. 

> This is why *not* [missing](/policy/#missing-a-lesson) a lesson is so important. It cannot simply be made up to doing something else online.