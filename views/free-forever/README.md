# Free Forever

The best things in life should be free, forever. Hording knowledge, love, and learning to make a buck is just wrong on so many levels. 

Sure everyone should be paid for what they do, but if what they do is create new ideas or help others develop new ideas then frankly they should be paid for their time and effort to help others acquire those ideas, but *not* for the idea itself. Ideas cannot be owned. 

I believe this extends to the very capturing of the ideas themselves, to the books, videos, and other forms of communicating the idea. It is not that the time to produce them and curate them is not valuable enough to pay for, its that by placing a dollar value on it you automatically eliminate entire populations and demographics from being exposed to those ideas. The worst of all are those who require their collections of other people's ideas to be only available as printed books with high price tags.

Thankfully the world is waking up to this reality.

> ***You will never be asked to pay for any of this content.*** Only for my time to help you understand it and learn from it in person.

