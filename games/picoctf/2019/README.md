---
Title: PicoCTF 2019
Query: true
---

First get access to a terminal. This stuff is plenty safe for you to do on your own computer if you prefer to download copies of the files. However, some *require* you to login to the <https://2019game.picoctf.com/shell>. 

:::co-pwz
You *can* login to the shell through `ssh` from your local system, but keep in mind that you will be doxing your IP to all 35k+ users of that system many of whom are interested in hacking. So, you ***probably*** want to be sure to use a VPN like [ProtonVPN](https://duck.com/lite?kae=t&q=ProtonVPN) or perhaps just use the [web interface](https://2019game.picoctf.com/shell) instead.

Copying the files down with `scp` certainly limits your exposure, but doesn't remove it.
:::

## Vault Door Training

Just need to open the `VaultDoorTraining.java` program and find the flag. Not that hard.

```sh
```
