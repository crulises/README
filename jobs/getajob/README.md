---
Title: Gain Trust to Get a Job
Subtitle: Again and Again
Query: true
---

Getting a job isn't hard if you are prepared, confident, and have contact with those who need what you have. You simply need to get someone to trust you enough to pay you by *proving* what you can do for them. Every single position and potential employer will have a different idea about what that proof looks like. That's it. 

Then, to keep your job you will need to continually affirm that trust by [proactively producing producing proof of your commitment](/advice/engage/) to solving *their* problems, not focusing on your own fascinations.

:::co-fyi
Over the years veteran technologists has usually encountered one or more peers who are obsessed with chasing the coolest thing, of implementing a new --- usually unproven --- technology mostly because *they* want to play with it --- *not* because the customer or organization *needs* it. These people will often make up reasons that such technology is "critical" when in fact they have only successfully convinced themselves of it enough to justify their selfish desire. When and if you encounter these types of technologist you should quietly do whatever you can to separate yourself from them before they take you down with them. They *always* fail and usually when they do is it with disastrous consequences for anyone else on that team. Often they will go [rogue](/what/cowboy/) and do it anyway.
:::

## Job Searching Tips

* Don't be passive
* Do the research
* Be on the hunt
* Get on Linkedin
* Always be closing
* Find a recruiter

## See Also

* [Elements of Programming Interviews](https://elementsofprogramminginterviews.com/)
