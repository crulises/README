---
Title: Site Reliability Engineer
Subtitle: Google's Stupid Title for System Administrators Who Codes
Query: true
---

> "[SRE is] what happens when a software engineer is tasked with what used to be called operations."

In other words, an SRE is a sysadmin who can code. (OMG, did your head explode from taking in all of that intense Google innovation?!) A *Site Reliability Engineer* is just a *good* [System Administrator](../sysadmin/), one who knows how to code and proactively develops automations, monitoring, and subsystems to keep things up and running.

:::co-rage
Google decided to come up with another stupid title, [write a book about it](https://github.com/songhuiqing/book/blob/master/OReilly.Site.Reliability.Engineering.2016.3.pdf), and market the hell out of it to fool everyone into thinking they've *invented* yet another new way of doing things (classic Google). Unfortunately, their stupid title (which doesn't even make sense) has become the one the industry uses. 

In reality, System Administration was being transformed all over the industry in several different companies. Google was *not* particularly special. Sure it was dealing with massive search engine requirements, but the scale and scope were not even particularly special or unique despite the main claims to the contrary (mostly by Googlers or ex-Googlers). Google just marketed it better. Those are the simple facts. IBM had initiatives in 2003 within Global Services to create a "Virtual Server Administrator" tool for auditing and automation well before Google decided to codify these practices into title. It is really too bad that the far more accurate title, System Administrator, has taken on a pejorative sense today. Yet another thing Google has ruined in the name of marketing themselves. An SRE is responsible for *way* more than just one "site" and limiting the title in that way is frankly offensive to the incredible amount of work to keep *all* systems running, not just Google's "site."
:::

