---
Title: '"Hey There" Challenge'
Tags: easy
---

Create a program that prints `Hey there` by default or `Hey there You` if `You` is
passed as an argument and `Hey there You over there` if `'You over there'`
is passed as the first argument (hint quotes).

## Search Hints

* bash variables
* bash command arguments

## Requirements

* Must include a *safe* shebang line if a script.
* When `./hey` then prints `Hey there`.
* When `./hey You` then prints `Hey there You`.
* When `./hey You over there` then prints `Hey there You`.
* When `./hey 'You over there'` then prints `Hey there You over there`.
* When *any* argument is passed print `Hey there <argument>`.
* Use the most efficient code possible.

## Bonus

* Write the algorithm first as comments using natural language.
* Validate using language test framework.
