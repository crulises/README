---
Title: Go Programming Language
SubTitle: Golang
---

Go is Python, Java, C/C++ replacement created at Google originally by
some of the most experienced engineers who have ever lived. It was
specifically created to handle *most* software development needs at
Google and has been tested by fire in the enterprise making it a
popular, ideal choice for most back-end work. These days Go should be
considered required learning by any serious technologist because of its
fast-growing ubiquity.

## Learning Go

Learning Go is one of the most rewarding activities you can pursue. The
best way to learn it is to start writing code in it, even the simplest
code. You can use the [language challenges](/lang/cha/) to get some
ideas.

However, when it comes to books, courses, and other materials there is
no real solid leader. Perhaps the best approach is to simply read the
documentation created by the Go team itself. Consider reading these in
the order suggested by the Go team:

1. [The Go Programming Language Specification](https://golang.org/ref/spec)
1. [Tour of Go](https://tour.golang.org/)
1. [How to Write Go Code](https://golang.org/doc/code.html)
1. [Effective Go](https://golang.org/doc/effective_go.html)

## See Also

* [Go Code Review Comments](https://github.com/golang/go/wiki/CodeReviewComments)
* [Ultimate Go](https://github.com/betty200744/ultimate-go)
* [Introduction to Go Programming](https://www.golang-book.com/)
* [The Little Go Book](https://openmymind.net/The-Little-Go-Book)
* [How to Code in
  Go](https://www.digitalocean.com/community/books/how-to-code-in-go-ebook)
* [Head First Go](https://headfirstgo.com)
* [Go Bootcamp](http://www.golangbootcamp.com/)
* [Learning Go](http://www.miek.nl/go)
* [Go for JavaScript Developers](https://github.com/pazams/go-for-javascript-developers)
* [Go in Action](https://www.manning.com/books/go-in-action)
* [Test-Driven Development with Go](https://leanpub.com/golang-tdd)
* [Go Secure Coding Practices](https://checkmarx.gitbooks.io/go-scp/content/)
* [Network Programming with Go](https://www.apress.com/us/book/9781484226919)
* [Go in Practice](https://www.manning.com/books/go-in-practice)
* [A Go Developer's Notebook](https://leanpub.com/GoNotebook/)
* [The Go Programming Language Phrasebook](https://www.informit.com/store/go-programming-language-phrasebook-9780321817143)
* [Black Hat Go](https://nostarch.com/blackhatgo)
* [Concurrency in Go](https://www.oreilly.com/library/view/concurrency-in-go/9781491941294/)
* [API Foundations in Go](https://leanpub.com/api-foundations)
* [Web Apps in Go, the Anti-Textbook](https://github.com/thewhitetulip/web-dev-golang-anti-textbook)
* [Go Web Programming](https://www.manning.com/books/go-web-programming)
* [Learn to Create Web Applications Using Go](https://www.usegolang.com/)
* [12 Factor Applications with Docker and Go](https://leanpub.com/12fa-docker-golang)
* [Build SaaS Apps in Go](https://buildsaasappingo.com)
* [Let's Go](https://lets-go.alexedwards.net/) 
* [Tutorial Edge Course](https://tutorialedge.net/course/golang/)
* [Programming with Google Go Specialization](https://www.coursera.org/specializations/google-golang/)
* <https://go.dev>
* <https://tour.golang.org> *(Despite the horrible JavaScript-only
  site.)*


