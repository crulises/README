---
Title: Beginner Boosts
Subtitle: Get Going Right

boosts/welcome
boosts/learn
boosts/essentials
boosts/codebook

- Title: Learn to Learn
  ID: learn
  Summary:
    Becoming an autodidact is the single most important skill you will ever
    learn. Your life literally depends on it, not to mention the entire [boost
    approach](/what/boost/).

- Title: Map the Tech Essentials
  ID: essentials
  Summary:
    Knowing what to know is a constant challenge in the field of technology.
    Here's a list of the modern essential tools, services, and languages you
    should learn with objective explanations about why you should learn them.

- Title: Create a Codebook
  ID: codebook
  Summary:
    It's a notebook with code, better than any overpriced textbook because
    *you* wrote it. Start taking notes and managing your knowledge in
    a sustainable way from the start so you can edit, add, search, and *share*
    your knowledge later. You never need to worry about word processor glitches
    and lock in. You can even turn your codebook into a [knowledge
    app](/what/knowledge/app/). You'll join a README.World community of authors
    who manage and share their [knowledge as source code](/what/knowledge/).

- Title: Write Basic Markdown
  ID: md/basic
  Summary: 
    [Markdown](/lang/md/) is the universal language for knowledge source. Use
    your [Basic Markdown](/lang/md/basic/) coding skills immediately all over
    the Internet including services like StackExchange, Reddit, Medium,
    Discord, GitHub, GitLab and many more. Use the Pandoc universal document
    conversion tool to render as HTML or *any* other format.

- Title: Publish to Netlify
  ID: netlify
  Summary:

- Title: Play with Code in REPL.it    
  ID: replit
  Summary:

- Title: Get a Linux Terminal, Stat!
  ID: getlinux
  Summary: 
    You'll learn every way possible to get access to the most powerful
    human-computer interface ever created including the cloud, virtual
    machines, and, of course, bare metal, big and small. By the end it won't be
    a question of *why* you need one any longer but *how soon you can get one*.

- Title: Know Your Keyboard
  ID: keyboard
  Summary: 
    Learn to *really* type.  Can you type a hashtag without looking at your
    [keyboard](/key/)? Can you find the `Escape` key? Apple was so 
    stupid they removed it. Understand just how idiotic that decision was and
    why Apple was *forced* to add it back.
  Items:
    - `Return`
    - `Enter`
    - `Escape`
    - `Control`
    - `Alt`
    - `Option`
    - `Super`
    - `Function`
    - `Fn1-19`
    - `PageUp`
    - `PageDown`
    - `Home`
    - `End`
    - `Insert`
    - `Backspace`
    - `Delete`
    - `Tab`
    - `CapsLock`
    - `Shift`
    - `;`
    - `:`
    - `\'`
    - `\"`
    - `\``
    - `~`
    - `\!`
    - `@`
    - `#`
    - `$`
    - `%`
    - `^`
    - `\&`
    - `\*`
    - `-`
    - `_`
    - `+`
    - `=`
    - `()`
    - `[]`
    - `{}`
    - `<>`
    - `|`
    - `\\`
    - `/`
    - `\?`
    - `C-[`
    - `C-c`
    - `C-d`
    - `C-s`
    - `C-z`
    - `C-q`
    - `C-a`
    - `C-l`

- Title: Know Your Mouse
  ID: mouse
  Summary: 
    The first mouse had three buttons. The left was for selecting. The middle
    was for pasting. Linux honors this *original* mouse usage that Apple's
    Larry Tesler idiotically defiled forcing the world to use both hands for
    something that never needed more than one.

- Title: Use Common Commands
  ID: common
  Summary:
    You have a big blank terminal screen and a blinking prompt. Now what? Keep
    calm and learn to enter commands. It's fun!
  Items:
    - `clear`
    - `date`
    - `cal`
    - `bc`
    - `apt`
    - `sl`
    - `tmatrix`
    - `lolcat`

- Title: List and Navigate Files
  ID: navigate
  Summary:
    Everything in Unix and Linux is just a different type of file. Learn to
    navigate the Linux filesystem from the Linux Bash command line interface
  Items: 
    - `ls`
    - `pwd`
    - `cd`
    - `./`
    - `../`
    - `*`
    - `**`
    - `?`
    - `{foo,bar}`
    - `{01..10}`

- Title: Create and Remove Files
  ID: files 
  Items:
    - `touch`
    - `mv`
    - `rm`
    - `rmdir` 

- Title: View Text Files
  ID: view
  Items:
    - `cat`
    - `head`
    - `tail`
    - `less`
    - `more`


    Every command you type is one more line of code
    in an ongoing interactive program that your helpful Bash interpreter is
    there to execute for you. Just tell it what to do. Put those commands in
    a file and you have a script you can run. Look at that! You're coding! 

---

[RWX]{.spy} beginner boosts get you started right. Since you're an [autodidact](/what/autodidact/) you'll want to take it from there. The goal is modern familiarity, not mastery; to help you avoid bad habits or ancient and irrelevant knowledge. The rest is up to you. Be sure to [pay it forward](/values/).

## Prerequisites

For those thinking about what they need --- or others whom they might help need --- here's a suggested list of prerequisites:

* Be able to read, write, and do math of most 16-year-olds
* Type 30 words per minute from home row
* Have a computer with a keyboard and a mouse
* Able to use a computer and graphical web browser
* Understand basic parts of a computer
    * CPU Microprocessor (Cores)
    * RAM Memory
    * Storage (Disk, SSD, USB Thumbdrives) 
    * Peripherals (Mouse, Keyboard, Screen)
* Have a compatible computer operating system
    * Windows 10 or above
    * Mac Sierra or above
    * Any Linux
    * No Chromebooks





 Survive Vi              Learn the minimum skills required to survive your first ecounter with
                         the world's oldest, most efficient, most ubiquitous, and 
                         least appreciated text editor on the planet. Yes, it's even easier
                         than Nano. You'll even get objective explanations of why Emacs
                         is a catastrophically bad pick for *most* [occupations](/jobs/).

 Connect with SSH        

 Git for Backups

 Screen and TMUX 

 Minimal Network
 Knowledge

 Structured Data         JSON, YAML, TOML, INI, XML, Properties, TSV, CSV, JQ

 Magical Vi Shell        Learn powerful magic muggles can never comprehend and even other
 Itegration              lesser wizards don't know --- especially those who actually pick
                         NeoVim or have dozens of Vim plugins as if they never learned the
                         Unix philosophy in the first place.

 Script with Bash

 Power of Perl
 Regular Expressions

 Find with Find

 Search with Lynx

 Tranfer with cURL       

 Integrate with GraphQL

 Publish with Pandoc

 Render as HTML

 Style with CSS

 Progressive JavaScript

 Encode and Unicode

 Contain with Docker

 Secure with KeePassXC

 TMUX Session Scripting

 Tab Complete All the
 Things

 Manage Knowledge
 with KN

 Share Knowledge on
 README.World

 Git Good with Git

 C for the Careful

 Golang for TUI Apps

 Rust Pest PEG Parsing

 Get Good to Gig

 Become a Prescient Pro

 Research and Reach Out
------------------------ --------------------------------------------------------------------------
