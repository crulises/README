---
Title: Course Logistics
Subtitle: How We Do Stuff
---

We are still working this out but we do know the following:

* Flipped
* Discord for communication
* Twitch for exposition and instruction
* YouTube for supplemental video 
* RWX.GG for all written material
