---
Title: Course Goals
Subtitle: What Will You Get
---

The goals for this course are rather simple. If everyone works together and you put in the time here's what you will get:

1. Empowerment and ownership of your learning, forever
1. Mastery of beginner coding and Linux skills
1. Proof

That's it, but it's enough. The rest is up to you.

