---
Title: RWX, Mantra of Ancient Autodidacts
Subtitle: Read/Research. Write. Execute/Exercise/Experience.
---

RWX is an ancient fix to a [corrupt](/what/education/corrupt/) and [irrelevant](/what/education/cantkeepup/) modern education system. Learning isn't that complicated. We humans have been doing it for thousands of years. But greed and power have corrupted and destroyed many of our systems of learning and sought to *control* our learning in ways that benefit everyone *but* the person learning.

The transfer of knowledge from one person to another is a rather simple practice. Those who master the practice become powerful [autodidacts](/what/autodidact/) capable of everything from convincing microorganisms to be our allies, to harnessing light and electricity to connect the world. Autodidacts get stuff done because they don't have to wait on anything or anyone to learn something new *now*, and they sure as hell don't ask for permission. They learn *despite* the system, not because of it.

* [**Executing, Exercising, and Experiencing**](./x/)  
We gain experience by doing things, trying things, making mistakes, and correcting ourselves, sometimes with the help of others who already have that experience.

* [**Writing**](./w/)  
We capture and share our learning experience through writing, image, and multimedia.

* [**Reading and Research**](./r/)  
We read and research what others have written about their learning and experiences and recreate them for ourselves in our own way.

This cycle repeats both individually and collectively. Usually we enter the cycle when we learn of others experience through reading. Hence, RWX.
