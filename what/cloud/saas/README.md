---
Title: What is Software as a Service / SaaS?
Subtitle: Apps You Use Over the Web Instead of Installing
Query: true
---

When you use a web app to read your email, schedule a meeting, or even watch a movie you are using *software as a service*. You don't install anything, you just use if over the Internet in the "cloud" as people say.

## See Also

* [Cloud Services](../)
