---
Title: To Teach Means to Show
Subtitle: Let's Take Back This Word
Query: true
---

> Hey! Teacher! Leave them kids alone!

The verb *to teach* has long lost its original meaning. These days most people think it means a transitive transfer of information from the *active* "teacher" to the *passive* "student" as in "sit down, shut up, listen and let me *teach* you." Too often traditional education has created a antagonistic relationship between the teacher and learner pitting one against the other rather than promoting a relationship of trust. It's a scientific fact that we learn better when are brains are flooded with dopamine --- promoted by love and trust --- and not cortisol --- brought from stress and fight or flight response.

This definition of `teaching` couldn't be farther from the original Old English tæcan meaning "to show, point out, declare, demonstrate" and "to give instruction, train, assign, direct; warn; persuade." The Proto-Germanic taikijan also meant "to show" and from the PIE root deik- "to show, point out." The root of the Old English is tacen, tacn "sign or mark."
