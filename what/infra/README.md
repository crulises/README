---
Title: What is Infrastructure?
Subtitle: The Stuff Everything Runs On
Query: true
---

In tech, *infrastructure* means the computers, devices, network, and other systems that allow [software applications](https://duck.com/lite?kae=t&q=software applications) to be installed, hosted, and maintained.

:::co-fyi
When used in a broader sense infrastructure can include roads, railways, airports, power lines, plumbing, garbage collection, Internet providers, and everything else that allows society to exist and function.
:::

[Infrastructure as a Service](/what/cloud/iaas/)

