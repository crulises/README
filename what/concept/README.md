---
Title: What is a Concept?
Subtitle: A General Idea or Understanding
Query: true
---

A *concept* is an idea or understanding about something. It involves terms but is more than any single term. In computer speak a concept is an aggregation of terms and other concepts.
