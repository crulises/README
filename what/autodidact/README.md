---
Title: Becoming an Autodidact
Subtitle: More Than Just Life-Long Learning
Query: true
---

> "I've come to the realization that Aaron learned how to learn at a very young age." [Noah Swartz](https://youtu.be/3Q6Fzbgs_Lg?t=175)

Becoming an autodidact is the most essential skill anyone will ever master, the earlier the better. Without it you simply cannot progress on your own. So many enter the workplace without learning this important skill. Perhaps you have encountered them. They tend to come over and ask a lot of questions, even coming off as needy and unprofessional. Don't be them. Learn to do your own research, answer your own questions, and really *look* at things.

On the other hand, those who have mastered learning early include the likes of [Aaron Swartz](/who/aaron/).

Autodidacts take learning into their own hands, either because they prefer it or are forced to for lack of other resources. De Vinci, Galileo, Ada Lovelace, Tesla, Wozniak, all were autodidacts. All followed a [simple formula](/what/rwx/) where mistakes are [sought out](/what/failfaster/) not shamed. 

Ironically few traditional schools spend any significant time at all helping you become an autodidact. It is assumed that you naturally will learn it as you are thrown into the traditional educational environment where you learn it or die. It is almost as if such organizations were motivated to make you dependent on *them* instead of yourself.

:::co-fun

## Uniquely Human Advantage 

Those who study humanity say that the collective ability to pass on knowledge to the next generation even after we have died is the single greatest reason humans have rocketed past all other species in growth and progress. Whether you believe this is because of natural science or some greater metaphysical force it is a fact we can observe.

:::

## See Also

* [Deep Work](https://www.amazon.com/Deep-Work-Focused-Success-Distracted/dp/1455586692)
