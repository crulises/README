---
Title: Content Map
Subtitle: How Stuff is Organized Here
Type: Table
Fields: [URL,Description]
Records:

- URL: map
  Description:
    This content map describing organization of this knowledge base.

- URL: articles
  Description:
    Blog or magazine-like articles on specific topics related to current events
    or just tech skills and work in general.

- URL: boosts
  Description:
    Giving you a push and then getting out of your way.
  
- URL: what
  Description:
    Definitions, terms, and concepts. "What is ...?"
  
- URL: who
  Description:
    Notable people you should know about, study, and follow.

- URL: tasks
  Description:
    Tasks with walkthroughs explaining how to complete them step-by-step.
    Useful for practicing the specific skills required to do the task.
    Sometimes called recipies. 

---


 `

 `.../cha/`       Small mneumonic mini-projects designed to solidify
                  learning about a specific skill, concept, or principle.
                  They are grouped with a specific tool, service, or
                  language and can be repeated as exercises or
                  self-assessments.

 `/tools/`        Commands, apps, programs, operating systems,
                  devices, hardware and other tools.

 `/services/`     Web, cloud, and local system services.

 `/q/`            Questions, frequently and not so frequently and ---
                  more importantly --- those that *should* be asked but
                  frequently are not.

 `/reviews/`      Reviews and limited annotations of curated books,
                  films, videos, sites, apps, people, and organizations.

 `/advice/dont/`       Seriously stupid stuff that has become so prevalent
                  it needs extra attention and blatent warnings
                  to counteract.

 `/contrib/`      Contributor information, style and content guidelines,
                  legal issues, donation information.

 `/jobs/`         Occupations, paid and otherwise as well as information
                  about getting and keeping a job.

 `/live/`         Information about live content including streaming
                  where you can get real-time help and information.

 `/copyright/`    Copyright coverage and trademarks.
----------------- -------------------------------------------------------

## Terms

* Who
  * Individuals
  * Organizations
* What
  * Terms
  * Tools
  * Services
  * Books
  * Videos
  * Films
  * Series
* When
  * Schedule
* Where
  * GitLab
  * Twitch
  * Discord
  * Twitter
  * Patreon
  * PayPal
  * YouTube
  * Vimeo?
* How
  * Tasks - Associated with Tool or Service, Recipie, HowTo
  * Exercises - TDD Driven, Precise Measurable Result, Can Be Timed
  * Projects - Open Ended, Creative, Flexible, Demonstratable (Portfolio)
  * Walkthroughs - Highly Specific, Precise Result and Target
  * Boosts - Minimal to Get Going
  * Courses - Comprehensive Coverage (Portfolio)

