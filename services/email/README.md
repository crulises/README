---
Title: Email Service Providers
Subtitle: Pick the Best for You, But Don't Discount Privacy
---

## Criteria

* Safety
* Privacy
* UX
* Customization
* Domains
* Reliability
* Spam
* API
* SMTP
* IMAP
* Integration
* Adoption
* Open
* Resources
* Features
* Price
* Media
* Blacklists
* Terminal

## Gmail.com

## ProtonMail.com

## Microsoft

### Hotmail.com

### Live.com

### Outlook.com

## Tutanota.com

## Yahoo.com

## AOL.com

## Host Your Own

