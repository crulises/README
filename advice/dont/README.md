---
Title: Just Don't Do This Stuff
Subtitle: Bad Advice You Should *Not* Heed
---

People will tell you to do a *lot* of things without having all the facts. Most mean well. Others not so much. Nothing makes it on this list that *isn't an objectively measurable and demonstrable fact.* Make sure you read them, even if you *choose* to not believe them.

* [Don't Read or Blog on Dev.to](./dodevto/)
* [Don't Hate on Perl](./perlhate/)
* [Don't Use Sed Unless Required](./sed/)
* [Don't Learn Emacs First](./emacs/)
* [Don't Use Zsh](./zsh/)
* [Don't License Under GPLv3](/views/gnuisdead)
* [Don't Control-L to Clear](./controll/)
* [Don't Cat to a Pipe](./catpipe/)
* [Don't Pipe to a Loop](./pipe2while/)
* [Don't Put Stuff in App Stores](./appstores/)
* [Don't Use `#!/usr/bin/env`](./env/)

:::co-faq

## Aren't These Just Your Opinions?

* 1 + 1 = 2
* The world is not flat. 
* Bash is the standard Linux shell, not Zsh.
* Emacs has to be installed.
* Sed has *no* substitution fields.
* Control-L doesn't always clear.
* `#!/usr/bin/env` is slow and unsafe.

These are not opinions. They are facts. *Thousands* of uninformed people not agreeing does not negate these facts no matter how painful the truth is or how *stupid* the world has become. You can choose to ignore these facts and give in to your [cognitive dissonance](/what/learning/cognitive/diss), but you can *never* change the fact that they are true. It is never mean or wrong to state the truth --- especially in the face of so much folly.
